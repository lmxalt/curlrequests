<?php
class curlReq
{ 
    public function displayUsers($id) {    //метод отображения пользователей
        $ch = curl_init(); //сеанс CURL
        if ($id == NULL){
            $url = "https://jsonplaceholder.typicode.com/users"; 
        } else $url = "https://jsonplaceholder.typicode.com/users/" . $id;  //проверка id при вызове метода 

        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true //получение ответа от сервера в $data
        ]);

        $data = curl_exec($ch);
        if($e = curl_error($ch)){ //вывод ошибки
            return $e;
        }
        else {
            return $data; //Возвращение данных о пользователях
        }
        curl_close($ch); //завершение сеанса CURL
    }
    public function displayPosts($userId) { //метод отображения постов
        $ch = curl_init();
        
        if ($userId == NULL){
            $url = "https://jsonplaceholder.typicode.com/posts";
        } else $url = "https://jsonplaceholder.typicode.com/posts?userId=" . $userId; //проверка userId при вызове метода 
         
        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true
        ]);
        $data = curl_exec($ch);
        if($e = curl_error($ch)){
            return $e;
        }
        else {
             return $data;
        }
        curl_close($ch);
    }
    public function displayTodos($userId) { //метод отображения заданий пользователей
        $ch = curl_init();

        if ($userId == NULL){
            $url = "https://jsonplaceholder.typicode.com/todos";
        } else $url = "https://jsonplaceholder.typicode.com/todos?userId=" . $userId;  

        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true
        ]);
        $data = curl_exec($ch);
        if($e = curl_error($ch)){
            return $e;
        }
        else {
            return $data;
        }
        curl_close($ch);
    }
   
    public function postPost($userId, $title, $body){ //метод добавления поста
        $ch = curl_init();
        $url = "https://jsonplaceholder.typicode.com/posts";

        $data_array = array( //значения передающиеся в поля
            'userId' => $userId,
            'id' => '',
            'title' => $title,
            'body' => $body
        );

        $data = http_build_query($data_array);

        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_POST => true,  //метод POST
            CURLOPT_POSTFIELDS => $data, //значения передаются в качестве полей
            CURLOPT_RETURNTRANSFER => true
        ]);

        $resp = curl_exec($ch);
        if($e = curl_error($ch)){
            return $e;
        }
        else {
            return $resp;
        }
        
        curl_close($ch);
    }
    
    public function updatePost($id, $title, $body){ //метод обновления поста
        $ch = curl_init();
        $url = "https://jsonplaceholder.typicode.com/posts/" . $id;
        
        $data_array = array( 
            'id' => $id,
            'title' => $title,
            'body' => $body
        );
        
        $data = http_build_query($data_array);

        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_RETURNTRANSFER => true
        ]);
      
        $resp = curl_exec($ch);
        if($e = curl_error($ch)){
            return $e;
        }
        else {
            
            return $resp;
        }
        curl_close($ch);
    }
    
    public function deletePost($id) { //метод удаления поста

    $url = "https://jsonplaceholder.typicode.com/posts/".$id;
    $ch = curl_init();
    
    curl_setopt_array($ch, [
        CURLOPT_URL => $url,
        CURLOPT_CUSTOMREQUEST => "DELETE",
    ]);
    
    $resp = curl_exec($ch);
    if($e = curl_error($ch)){
        return $e;
    }
    else {
        return $resp;
    }
    curl_close($ch);
    }
}

?>